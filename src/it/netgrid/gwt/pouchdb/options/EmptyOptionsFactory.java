package it.netgrid.gwt.pouchdb.options;

public class EmptyOptionsFactory implements IPouchdbOptionsFactory {

	@Override
	public final native ViewQueryOptions buildViewQueryOptions() /*-{
		return {};
	}-*/;

	
	@Override
	public final native AllDocsOptions buildAllDocsOptions() /*-{
		return {};
	}-*/;

	@Override
	public final native ChangesOptions buildChangesOptions() /*-{
		return {};
	}-*/;

	@Override
	public final native DbConnectionOptions buildDbConnectionOptions() /*-{
		return {};
	}-*/;

	@Override
	public final native GetDocOptions buildGetDocOptions() /*-{
		return {};
	}-*/;

	@Override
	public final native ReplicationOptions buildReplicationOptions() /*-{
		return {};
	}-*/;


	@Override
	public final native FilterQueryParams buildFilterQueryParams() /*-{
	return {};
}-*/;

}
