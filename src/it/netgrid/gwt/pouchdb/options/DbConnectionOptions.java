package it.netgrid.gwt.pouchdb.options;

import com.google.gwt.core.client.JavaScriptObject;

public class DbConnectionOptions extends JavaScriptObject {
	
	protected DbConnectionOptions() {
	}
	
	public final native String getName() /*-{
		return this.name;
	}-*/;
	
	public final native void setName(String dbName) /*-{
		this.name = dbName;
	}-*/;
	
	public final native boolean isAutoCompatcion()  /*-{
		return this.auto_compaction == true;
	}-*/;
	
	public final native void setAutoCompaction(boolean autoCompact) /*-{
		this.auto_compaction = autoCompact;
	}-*/;	
}
