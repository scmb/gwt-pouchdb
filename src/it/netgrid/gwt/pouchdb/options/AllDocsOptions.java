package it.netgrid.gwt.pouchdb.options;

import it.netgrid.gwt.pouchdb.ArrayUtils;

import java.util.List;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArrayString;

public class AllDocsOptions extends JavaScriptObject {
	
	protected AllDocsOptions() {}
	
	public final native void setIncludeDocs(boolean includeDocs) /*-{
		this.include_docs = includeDocs;
	}-*/;
	
	public final native boolean isIncludeDocs() /*-{
		return this.include_docs == true;
	}-*/;
	
	public final native void setStartKey(String startKey) /*-{
		this.startkey = startKey;
	}-*/;
	
	public final native String getStartKey() /*-{
		return this.startkey;
	}-*/;
	
	public final native void setEndKey(String endKey) /*-{
		this.endkey = endKey;
	}-*/;
	
	public final native String getEndKey() /*-{
		return this.endkey;
	}-*/;
	
	public final native void setDescending(boolean descending) /*-{
		this.descending = descending;
	}-*/;
	
	public final native boolean isDescending() /*-{
		return this.descending;
	}-*/;
	
	public final native void setKeys(JsArrayString keys) /*-{
		this.keys = keys;
	}-*/;

	public final void setKeys(List<String> keys) {
		this.setKeys(ArrayUtils.toJsArrayString(keys));
	}
	
	public final native JsArrayString getKeys() /*-{
		return this.keys;
	}-*/;
	
	public final native void setAttachments(boolean attachments) /*-{
		this.attachments = attachments;
	}-*/;
	
	public final native boolean isAttachments() /*-{
		return this.attachments;
	}-*/;
}
