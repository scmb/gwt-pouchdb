package it.netgrid.gwt.pouchdb.options;

public interface IPouchdbOptionsFactory {
	public AllDocsOptions buildAllDocsOptions();

	public ChangesOptions buildChangesOptions();

	public DbConnectionOptions buildDbConnectionOptions();

	public GetDocOptions buildGetDocOptions();
	
	public ReplicationOptions buildReplicationOptions();

	public ViewQueryOptions buildViewQueryOptions();
	
	public FilterQueryParams buildFilterQueryParams();
}
