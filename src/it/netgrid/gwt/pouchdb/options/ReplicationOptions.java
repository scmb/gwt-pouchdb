package it.netgrid.gwt.pouchdb.options;

import it.netgrid.gwt.pouchdb.handler.IDocChangeEventHandler;
import it.netgrid.gwt.pouchdb.handler.IReplicationCompleteHandler;

import com.google.gwt.core.client.JavaScriptObject;

public class ReplicationOptions extends JavaScriptObject {
	protected ReplicationOptions() {}
	public final native void setFilter(String filter) /*-{
		this.filter = filter;
	}-*/;
	public final native String getFilter() /*-{
		return this.filter;
	}-*/;
	public final native void setComplete(IReplicationCompleteHandler callback) /*-{
		this.complete = function(err, data) {
			if(err != null)
				callback.@it.netgrid.gwt.pouchdb.handler.IReplicationCompleteHandler::onSuccess(Lit/netgrid/gwt/pouchdb/ReplicationResult;)(data);
			else
				callback.@it.netgrid.gwt.pouchdb.handler.IReplicationCompleteHandler::onError(Lit/netgrid/gwt/pouchdb/response/ErrorResponse;)(err);
		}
	}-*/;
	public final native void setOnChange(IDocChangeEventHandler callback) /*-{}-*/;
	public final native void setContinuous(boolean continuous) /*-{}-*/;
	public final native boolean isContinuous() /*-{}-*/;
}