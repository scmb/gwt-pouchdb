package it.netgrid.gwt.pouchdb;

import it.netgrid.gwt.pouchdb.handler.IAllDocsHandler;
import it.netgrid.gwt.pouchdb.handler.ICompactionHandler;
import it.netgrid.gwt.pouchdb.handler.IDbInfoHandler;
import it.netgrid.gwt.pouchdb.handler.IDocGetHandler;
import it.netgrid.gwt.pouchdb.handler.IDocUpdateHandler;
import it.netgrid.gwt.pouchdb.handler.IQueryMixedResultHandler;
import it.netgrid.gwt.pouchdb.handler.IQueryResultHandler;
import it.netgrid.gwt.pouchdb.handler.IViewQueryMixedResultHandler;
import it.netgrid.gwt.pouchdb.handler.IViewQueryResultHandler;
import it.netgrid.gwt.pouchdb.options.AllDocsOptions;
import it.netgrid.gwt.pouchdb.options.ChangesOptions;
import it.netgrid.gwt.pouchdb.options.CompactionOptions;
import it.netgrid.gwt.pouchdb.options.GetDocOptions;
import it.netgrid.gwt.pouchdb.options.ViewQueryOptions;
import it.netgrid.gwt.pouchdb.response.QueryResponse;

import java.util.List;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;

public interface IPouchDb {
	public void allDocs(IAllDocsHandler callback);

	public void allDocs(IAllDocsHandler callback, AllDocsOptions options);

	public <D extends PouchDbDoc> void bulkDocs(JsArray<D> docs);

	public <D extends PouchDbDoc> void bulkDocs(JsArray<D> docs, IDocUpdateHandler callback);

	public <D extends PouchDbDoc> void bulkDocs(List<D> docs);

	public <D extends PouchDbDoc> void bulkDocs(List<D> docs, IDocUpdateHandler callback);

	public ChangesFeed changes(ChangesOptions options);

	public void compact();

	public void compact(ICompactionHandler callback);

	public void compact(ICompactionHandler callback, CompactionOptions options);

	public void get(String id, IDocGetHandler<?> callback);

	public void get(String id, IDocGetHandler<?> callback, GetDocOptions options);

	public void info(IDbInfoHandler callback);

	public void post(PouchDbDoc doc);

	public void post(PouchDbDoc doc, IDocUpdateHandler callback);

	public void put(PouchDbDoc doc);

	public void put(PouchDbDoc doc, IDocUpdateHandler callback);

	public void query(AQueryMixed query, IQueryMixedResultHandler callback);

	public <D extends JavaScriptObject> void query(AQuery<D,?,?> query, IQueryResultHandler<D> callback);

	public void remove(PouchDbDoc doc);

	public void remove(PouchDbDoc doc, IDocUpdateHandler callback);

	public void queryView(String view, ViewQueryOptions options, IViewQueryResultHandler<?> callback);

	public void queryView(String view, IViewQueryResultHandler<?> callback);

	public void queryView(String view, IViewQueryMixedResultHandler callback);
	
	public void queryView(String view, ViewQueryOptions options, IViewQueryMixedResultHandler callback);
	
	public <D extends JavaScriptObject> List<D> arrayToList(JsArray<D> array);

	public <D extends JavaScriptObject> List<D> responseToList(QueryResponse<D> response);
}
