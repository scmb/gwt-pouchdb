package it.netgrid.gwt.pouchdb;

import com.google.gwt.core.client.JavaScriptObject;

public class ReplicationResult extends JavaScriptObject {
	protected ReplicationResult() {
	}

	public final native boolean isOk() /*-{
		return this.ok;
	}-*/;

	public final native void setOk(boolean ok) /*-{
		this.ok = ok;
	}-*/;

	public final native int getDocsRead() /*-{
		return this.docs_read;
	}-*/;

	public final native void setDocsRead(int docsRead) /*-{
		this.docs_read = docsRead;
	}-*/;

	public final native int getDocsWritten() /*-{
		return this.docs_written;
	}-*/;

	public final native void setDocsWritten(int docsWritten) /*-{
		this.docs_written = docsWritte;
	}-*/;

	public final native String getStartTime() /*-{
		return this.start_time;
	}-*/;

	public final native void setStartTime(String startTime) /*-{
		this.start_time = startTime;
	}-*/;

	public final native String getEndTime() /*-{
		return this.end_time;
	}-*/;

	public final native void setEndTime(String endTime) /*-{
		this.end_time = endTime;
	}-*/;
}
