package it.netgrid.gwt.pouchdb;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArrayMixed;

public abstract class AQueryMixed {

	protected final native static void emit(String key, JavaScriptObject value) /*-{
		emit(key, value);
	}-*/;

	protected PouchDbDoc getDoc(int index, JsArrayMixed array) {
		JavaScriptObject item = array.getObject(index);
		return item == null ? (PouchDbDoc) null : item.<PouchDbDoc> cast();
	}

	protected <T extends JavaScriptObject> T getItem(int index,
			JsArrayMixed array) {
		JavaScriptObject item = array.getObject(index);
		return item == null ? (T) null : item.<T> cast();
	}

	public abstract ReductionType getReductionType();

	public abstract void map(PouchDbDoc doc);

	public abstract void reduce(JsArrayMixed keys, JsArrayMixed values,
			boolean rereduce);
}
