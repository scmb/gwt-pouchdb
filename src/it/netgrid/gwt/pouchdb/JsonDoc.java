package it.netgrid.gwt.pouchdb;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArrayBoolean;
import com.google.gwt.core.client.JsArrayInteger;
import com.google.gwt.core.client.JsArrayMixed;
import com.google.gwt.core.client.JsArrayNumber;
import com.google.gwt.core.client.JsArrayString;

public class JsonDoc extends JavaScriptObject {
	protected JsonDoc() {}

	public final native String getString(String key) /*-{
		return this[key];
	}-*/;

	public final native int getInteger(String key) /*-{
		return this[key];
	}-*/;

	public final native boolean getBoolean(String key) /*-{
		return this[key];
	}-*/;

	public final native JavaScriptObject getObject(String key) /*-{
		return this[key];
	}-*/;

	public final native JsArrayBoolean getJsArrayBoolean(String key) /*-{
		return this[key];
	}-*/;

	public final native JsArrayInteger getJsArrayInteger(String key) /*-{
		return this[key];
	}-*/;

	public final native JsArrayString getJsArrayString(String key) /*-{
		return this[key];
	}-*/;

	public final native JsArrayNumber getJsArrayNumber(String key) /*-{
		return this[key];
	}-*/;

	public final native JsArrayMixed getJsArrayMixed(String key) /*-{
		return this[key];
	}-*/;
}
