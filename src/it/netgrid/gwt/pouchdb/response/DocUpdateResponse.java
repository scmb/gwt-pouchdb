package it.netgrid.gwt.pouchdb.response;

import com.google.gwt.core.client.JavaScriptObject;

public class DocUpdateResponse extends JavaScriptObject {
	protected DocUpdateResponse() {
	}

	public final native String getId() /*-{
		return this.id;
	}-*/;

	public final native void setId(String id) /*-{
		this.id = id;
	}-*/;

	public final native String getRev() /*-{
		return this.rev;
	}-*/;

	public final native void setRev(String rev) /*-{
		this.rev = rev;
	}-*/;

	public final native boolean isOk() /*-{
		return this.ok;
	}-*/;

	public final native void setOk(boolean ok) /*-{
		this.ok = ok;
	}-*/;
}
