package it.netgrid.gwt.pouchdb.response;

import it.netgrid.gwt.pouchdb.RevisionReference;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;

public class DocChangeEvent extends JavaScriptObject {
	protected DocChangeEvent() {
	}

	public final native String getId() /*-{
		return this.id;
	}-*/;

	public final native int getSeq() /*-{
		return this.seq;
	}-*/;

	public final native JsArray<RevisionReference> getChanges() /*-{
		return this.changes;
	}-*/;

	public final native JavaScriptObject getDoc() /*-{
		return this.doc;
	}-*/;

	public final native RevisionReference getValue() /*-{
		return this.value;
	}-*/;
}
