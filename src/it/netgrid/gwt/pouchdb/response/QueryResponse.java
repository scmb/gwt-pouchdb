package it.netgrid.gwt.pouchdb.response;

import it.netgrid.gwt.pouchdb.QueryResponseRow;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;

public class QueryResponse<T extends JavaScriptObject> extends JavaScriptObject {

	protected QueryResponse() {}
	
	public final native JsArray<QueryResponseRow<T>> getRows() /*-{
		return this.rows;
	}-*/;
	
	public final native int getOffset() /*-{
		return this.offset;
	}-*/;
	
	public final native int getTotalRows() /*-{
		return this.total_rows;
	}-*/;

}
