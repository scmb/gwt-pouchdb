package it.netgrid.gwt.pouchdb.response;

import com.google.gwt.core.client.JavaScriptObject;

public class ErrorResponse extends JavaScriptObject {
	protected ErrorResponse() {}
	
	public final native boolean getError() /*-{
		return this.error;
	}-*/;

	public final native String getReason() /*-{
		return this.reason;
	}-*/;

	public final native int getStatus()/*-{
		return this.status;
	}-*/;

}
