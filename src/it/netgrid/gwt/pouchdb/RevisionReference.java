package it.netgrid.gwt.pouchdb;

import com.google.gwt.core.client.JavaScriptObject;

public class RevisionReference extends JavaScriptObject {
	protected RevisionReference() {
	}

	public final native String getId() /*-{
		return this.rev;
	}-*/;
}
