package it.netgrid.gwt.pouchdb.handler;

import it.netgrid.gwt.pouchdb.response.DocUpdateResponse;
import it.netgrid.gwt.pouchdb.response.ErrorResponse;

import com.google.gwt.core.client.JavaScriptObject;

public interface IDocUpdateHandler {
	public void onSuccess(DocUpdateResponse response);

	public void onError(ErrorResponse error);
}