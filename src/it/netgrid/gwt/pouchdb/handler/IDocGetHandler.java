package it.netgrid.gwt.pouchdb.handler;

import it.netgrid.gwt.pouchdb.PouchDbDoc;
import it.netgrid.gwt.pouchdb.response.ErrorResponse;

public interface IDocGetHandler<D extends PouchDbDoc> {

	public void onSuccess(D doc);

	public void onError(ErrorResponse error);
}
