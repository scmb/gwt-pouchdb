package it.netgrid.gwt.pouchdb.handler;

import it.netgrid.gwt.pouchdb.response.ErrorResponse;

import com.google.gwt.core.client.JavaScriptObject;

public interface IGenericHandler {

	public void onSuccess(JavaScriptObject response);

	public void onError(ErrorResponse error);
}
