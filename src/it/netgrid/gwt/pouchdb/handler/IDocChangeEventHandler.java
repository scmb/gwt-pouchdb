package it.netgrid.gwt.pouchdb.handler;

import it.netgrid.gwt.pouchdb.response.DocChangeEvent;

public interface IDocChangeEventHandler {
	public void onChange(DocChangeEvent data);
}
