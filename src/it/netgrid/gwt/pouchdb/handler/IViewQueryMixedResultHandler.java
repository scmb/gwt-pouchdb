package it.netgrid.gwt.pouchdb.handler;

import it.netgrid.gwt.pouchdb.response.ErrorResponse;
import it.netgrid.gwt.pouchdb.response.MixedQueryResponse;

public interface IViewQueryMixedResultHandler {
	public void onError(ErrorResponse error);

	public void onSuccess(MixedQueryResponse response);
}
