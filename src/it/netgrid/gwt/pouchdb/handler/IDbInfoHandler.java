package it.netgrid.gwt.pouchdb.handler;

import it.netgrid.gwt.pouchdb.response.DbInfoResponse;
import it.netgrid.gwt.pouchdb.response.ErrorResponse;

import com.google.gwt.core.client.JavaScriptObject;

public interface IDbInfoHandler {
	public void onSuccess(DbInfoResponse response);

	public void onError(ErrorResponse error);
}
