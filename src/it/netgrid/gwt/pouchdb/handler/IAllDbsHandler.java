package it.netgrid.gwt.pouchdb.handler;

import it.netgrid.gwt.pouchdb.response.ErrorResponse;

import com.google.gwt.core.client.JsArrayString;

public interface IAllDbsHandler {
	public void onSuccess(JsArrayString dbs);

	public void onError(ErrorResponse err);
}
