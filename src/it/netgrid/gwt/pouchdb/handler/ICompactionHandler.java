package it.netgrid.gwt.pouchdb.handler;

public interface ICompactionHandler {
	public void onCompactionDone();
}