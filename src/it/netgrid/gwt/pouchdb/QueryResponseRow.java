package it.netgrid.gwt.pouchdb;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.JsArrayMixed;

public class QueryResponseRow<T extends JavaScriptObject> extends JavaScriptObject {
	protected QueryResponseRow() {}
	
	public final native String getId()/*-{
		return this.id;
	}-*/;
	
	public final native void setId(String id) /*-{
		this.id = id;
	}-*/;
	
	public final native JsArrayMixed getKeyAsArray() /*-{
		return this.key;
	}-*/;
	
	public final native JavaScriptObject getKey() /*-{
		return this.key;
	}-*/;
	
	public final native void setKey(JavaScriptObject key) /*-{
		this.key = key;
	}-*/;
	
	public final native void setKey(JsArrayMixed key) /*-{
		this.key = key;
	}-*/;

	
	public final native T getValue() /*-{
		return this.value;
	}-*/;
	
	public final native void setValue(T value) /*-{
		this.value = value;
	}-*/;

}
