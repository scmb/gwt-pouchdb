package it.netgrid.gwt.pouchdb;


public class PouchDbDoc extends JsonDoc {
	protected PouchDbDoc() {
	}

	public final native String getId() /*-{
		return this._id;
	}-*/;

	public final native void setId(String id) /*-{
		this._id = id;
	}-*/;

	public final native String getRev() /*-{
		return this._rev;
	}-*/;

	public final native void setRev(String rev) /*-{
		this._rev = rev;
	}-*/;

	public final native boolean isDeleted() /*-{
		return this._deleted == true;
	}-*/;

	public final native void setDeleted(boolean value) /*-{
		this._deleted = value;
	}-*/;
}
