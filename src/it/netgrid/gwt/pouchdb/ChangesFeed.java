package it.netgrid.gwt.pouchdb;

import com.google.gwt.core.client.JavaScriptObject;

public class ChangesFeed extends JavaScriptObject {
	protected ChangesFeed() {
	}

	public final native void cancel() /*-{
		this.cancel();
	}-*/;
}