package it.netgrid.gwt.pouchdb;

import java.util.List;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.JsArrayString;

public class ArrayUtils {

	public static <D extends PouchDbDoc> JsArray<D> toJsArrayDocs(List<D> list) {
		JsArray<D> retval = JavaScriptObject.createArray().cast();
		for (D item : list) {
			retval.push(item);
		}
		return retval;
	}

	public static <D extends PouchDbDoc> JsArray<D> toJsArray(
			List<D> list) {
		JsArray<D> retval = JavaScriptObject.createArray()
				.cast();
		for (D item : list) {
			retval.push(item);
		}

		return retval;
	}

	public static JsArrayString toJsArrayString(List<String> list) {
		JsArrayString retval = JavaScriptObject.createArray().cast();
		for (String item : list) {
			retval.push(item);
		}

		return retval;
	}
}
