package it.netgrid.gwt.pouchdb;

public enum ReductionType {
	NONE,
	SUM,
	COUNT,
	STATS,
	CUSTOM;
}
